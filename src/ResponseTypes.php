<?php

namespace WarehouseX\Client;

use OpenAPI\Runtime\ResponseTypes as AbstractResponseTypes;

class ResponseTypes extends AbstractResponseTypes
{
    public array $types = [
        'getClientAddressBookCollection' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientAddressBook\\ClientAddressBook\\Read[]',
        ],
        'postClientAddressBookCollection' => [
            '201.' => 'WarehouseX\\Client\\Model\\ClientAddressBook\\ClientAddressBook\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientAddressBookItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientAddressBook\\ClientAddressBook\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientAddressBookItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientAddressBook\\ClientAddressBook\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientAddressBookItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientLevelDiscountCollection' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevelDiscount\\ClientLevelOutput\\ClientLevel\\Read[]',
        ],
        'postClientLevelDiscountCollection' => [
            '201.' => 'WarehouseX\\Client\\Model\\ClientLevelDiscount\\ClientLevelOutput\\ClientLevel\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientLevelDiscountItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevelDiscount\\ClientLevelOutput\\ClientLevel\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientLevelDiscountItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevelDiscount\\ClientLevelOutput\\ClientLevel\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientLevelDiscountItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientLevelCollection' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevel\\ClientLevelOutput\\ClientLevel\\Read[]',
        ],
        'postClientLevelCollection' => [
            '201.' => 'WarehouseX\\Client\\Model\\ClientLevel\\ClientLevelOutput\\ClientLevel\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientLevelItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevel\\ClientLevelOutput\\ClientLevel\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientLevelItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevel\\ClientLevelOutput\\ClientLevel\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientLevelItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_client_levels_client_level_discounts_get_subresourceClientLevelSubresource' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevelDiscount\\ClientLevelOutput\\ClientLevel\\Read[]',
        ],
        'getClientCollection' => [
            '200.' => 'WarehouseX\\Client\\Model\\Client\\ClientOutput\\Client\\Read[]',
        ],
        'postClientCollection' => [
            '201.' => 'WarehouseX\\Client\\Model\\Client\\ClientOutput\\Client\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'getClientItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\Client\\ClientOutput\\Client\\Read',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'putClientItem' => [
            '200.' => 'WarehouseX\\Client\\Model\\Client\\ClientOutput\\Client\\Read',
            '400.' => 'OpenAPI\\Runtime\\GenericResponse',
            '422.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'deleteClientItem' => [
            '204.' => 'OpenAPI\\Runtime\\GenericResponse',
            '404.' => 'OpenAPI\\Runtime\\GenericResponse',
        ],
        'api_clients_client_level_get_subresourceClientSubresource' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevel\\ClientLevelOutput\\ClientLevel\\Read',
        ],
        'api_clients_client_level_client_level_discounts_get_subresourceClientSubresource' => [
            '200.' => 'WarehouseX\\Client\\Model\\ClientLevelDiscount\\ClientLevelOutput\\ClientLevel\\Read[]',
        ],
    ];
}
