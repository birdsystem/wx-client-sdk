<?php

namespace WarehouseX\Client\Model\ClientAddressBook\ClientAddressBook;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientAddressBook.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $company = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $address = null;

    /**
     * @var string|null
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var string
     */
    public $createTime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     */
    public $client = null;
}
