<?php

namespace WarehouseX\Client\Model\ClientAddressBook\ClientAddressBookInput\ClientAddressBook;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientAddressBook.
 */
class Put extends AbstractModel
{
    /**
     * @var string
     */
    public $contact = null;

    /**
     * @var string
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $company = null;

    /**
     * @var string|null
     */
    public $city = null;

    /**
     * @var string|null
     */
    public $county = null;

    /**
     * @var string
     */
    public $address = null;

    /**
     * @var string|null
     */
    public $countryIso = null;

    /**
     * @var string|null
     */
    public $postcode = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
