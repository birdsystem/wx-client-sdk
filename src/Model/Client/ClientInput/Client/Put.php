<?php

namespace WarehouseX\Client\Model\Client\ClientInput\Client;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Client.
 */
class Put extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string
     */
    public $clientLevel = null;

    /**
     * @var string|null
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string|null
     */
    public $address = null;

    /**
     * @var int|null
     */
    public $userIdSales = null;

    /**
     * @var int|null
     */
    public $userIdCustomer = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
