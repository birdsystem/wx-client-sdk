<?php

namespace WarehouseX\Client\Model\Client\ClientOutput\Client;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * Client.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var \WarehouseX\Client\Model\ClientLevel\ClientLevelOutput\Client\Read
     */
    public $clientLevel = null;

    /**
     * @var string|null
     */
    public $contact = null;

    /**
     * @var string|null
     */
    public $telephone = null;

    /**
     * @var string|null
     */
    public $email = null;

    /**
     * @var string|null
     */
    public $address = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var int|null
     */
    public $userIdAdmin = null;

    /**
     * @var int|null
     */
    public $userIdSales = null;

    /**
     * @var int|null
     */
    public $userIdCustomer = null;

    /**
     * @var string
     */
    public $status = null;

    /**
     * @var string[]
     */
    public $attachments = null;

    public $userAdmin = null;

    public $userSales = null;

    public $userCustomer = null;

    /**
     * @var string
     */
    public $createTime = null;

    /**
     * @var string
     */
    public $updateTime = null;
}
