<?php

namespace WarehouseX\Client\Model\ClientLevelDiscountInput\ClientLevel;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

class Write extends AbstractModel
{
    /**
     * @var string
     */
    public $feeType = null;

    /**
     * @var float
     */
    public $discount = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';
}
