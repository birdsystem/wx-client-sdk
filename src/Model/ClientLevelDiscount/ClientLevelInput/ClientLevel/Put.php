<?php

namespace WarehouseX\Client\Model\ClientLevelDiscount\ClientLevelInput\ClientLevel;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientLevelDiscount.
 */
class Put extends AbstractModel
{
    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $currencyCode = null;

    /**
     * @var float
     */
    public $amount = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    /**
     * @var \WarehouseX\Client\Model\ClientLevelDiscountInput\ClientLevel\Put[]
     */
    public $clientLevelDiscounts = null;
}
