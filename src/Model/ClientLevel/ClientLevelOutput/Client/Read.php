<?php

namespace WarehouseX\Client\Model\ClientLevel\ClientLevelOutput\Client;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientLevel.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $currencyCode = null;

    /**
     * @var float
     */
    public $amount = null;
}
