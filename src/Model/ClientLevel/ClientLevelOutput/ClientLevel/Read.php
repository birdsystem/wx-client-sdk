<?php

namespace WarehouseX\Client\Model\ClientLevel\ClientLevelOutput\ClientLevel;

use OpenAPI\Runtime\AbstractModel as AbstractModel;

/**
 * ClientLevel.
 */
class Read extends AbstractModel
{
    /**
     * @var int
     */
    public $id = null;

    /**
     * @var string
     */
    public $name = null;

    /**
     * @var string|null
     */
    public $note = null;

    /**
     * @var string
     */
    public $currencyCode = null;

    /**
     * @var float
     */
    public $amount = null;

    /**
     * @var string
     */
    public $status = 'ACTIVE';

    public $createTime = null;

    public $clientLevelDiscounts = null;
}
