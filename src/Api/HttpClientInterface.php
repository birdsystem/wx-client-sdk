<?php

namespace WarehouseX\Client\Api;

use Psr\Http\Client\ClientInterface as BaseClass;

interface HttpClientInterface extends BaseClass
{
}
