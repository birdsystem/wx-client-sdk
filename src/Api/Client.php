<?php

namespace WarehouseX\Client\Api;

use WarehouseX\Client\Model\Client\ClientInput\Client\Put as Put;
use WarehouseX\Client\Model\Client\ClientInput\Client\Write as Write;
use WarehouseX\Client\Model\Client\ClientOutput\Client\Read as Read;

class Client extends AbstractAPI
{
    /**
     * Retrieves the collection of Client resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'name'	string
     *                       'contact'	string
     *                       'telephone'	string
     *                       'email'	string
     *                       'email[]'	array
     *                       'userIdAdmin'	integer
     *                       'userIdAdmin[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'type'	string
     *                       'type[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'updateTime[before]'	string
     *                       'updateTime[strictly_before]'	string
     *                       'updateTime[after]'	string
     *                       'updateTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *                       'order[updateTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientCollection',
        'GET',
        'api/client/clients',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a Client resource.
     *
     * @param Write $Model The new Client resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postClientCollection',
        'POST',
        'api/client/clients',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a Client resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientItem',
        'GET',
        "api/client/clients/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the Client resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated Client resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putClientItem',
        'PUT',
        "api/client/clients/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the Client resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientItem',
        'DELETE',
        "api/client/clients/$id",
        null,
        [],
        []
        );
    }
}
