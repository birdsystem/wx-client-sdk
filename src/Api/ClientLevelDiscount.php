<?php

namespace WarehouseX\Client\Api;

use WarehouseX\Client\Model\ClientLevelDiscount\ClientLevelInput\ClientLevel\Put as Put;
use WarehouseX\Client\Model\ClientLevelDiscount\ClientLevelInput\ClientLevel\Write as Write;
use WarehouseX\Client\Model\ClientLevelDiscount\ClientLevelOutput\ClientLevel\Read as Read;

class ClientLevelDiscount extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientLevelDiscount resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientLevelDiscountCollection',
        'GET',
        'api/client/client_level_discounts',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientLevelDiscount resource.
     *
     * @param Write $Model The new ClientLevelDiscount resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postClientLevelDiscountCollection',
        'POST',
        'api/client/client_level_discounts',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientLevelDiscount resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientLevelDiscountItem',
        'GET',
        "api/client/client_level_discounts/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientLevelDiscount resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated ClientLevelDiscount resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putClientLevelDiscountItem',
        'PUT',
        "api/client/client_level_discounts/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientLevelDiscount resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientLevelDiscountItem',
        'DELETE',
        "api/client/client_level_discounts/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a ClientLevel resource.
     *
     * @param string $id      ClientLevel identifier
     * @param array  $queries options:
     *                        'page'	integer	The collection page number
     *                        'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function api_client_levels_client_level_discounts_get_subresourceClientLevelSubresource(string $id, array $queries = []): ?array
    {
        return $this->request(
        'api_client_levels_client_level_discounts_get_subresourceClientLevelSubresource',
        'GET',
        "api/client/client_levels/$id/client_level_discounts",
        null,
        $queries,
        []
        );
    }

    /**
     * Retrieves a Client resource.
     *
     * @param string $id          Client identifier
     * @param string $clientLevel ClientLevel identifier
     * @param array  $queries     options:
     *                            'page'	integer	The collection page number
     *                            'itemsPerPage'	integer	The number of items per page
     *
     * @return Read[]|null
     */
    public function api_clients_client_level_client_level_discounts_get_subresourceClientSubresource(string $id, string $clientLevel, array $queries = []): ?array
    {
        return $this->request(
        'api_clients_client_level_client_level_discounts_get_subresourceClientSubresource',
        'GET',
        "api/client/clients/$id/client_level/client_level_discounts",
        null,
        $queries,
        []
        );
    }
}
