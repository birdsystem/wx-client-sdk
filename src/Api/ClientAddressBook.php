<?php

namespace WarehouseX\Client\Api;

use WarehouseX\Client\Model\ClientAddressBook\ClientAddressBook\Read as Read;
use WarehouseX\Client\Model\ClientAddressBook\ClientAddressBookInput\ClientAddressBook\Input as Input;
use WarehouseX\Client\Model\ClientAddressBook\ClientAddressBookInput\ClientAddressBook\Put as Put;

class ClientAddressBook extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientAddressBook resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'client'	string
     *                       'client[]'	array
     *                       'contact'	string
     *                       'contact[]'	array
     *                       'telephone'	string
     *                       'telephone[]'	array
     *                       'company'	string
     *                       'company[]'	array
     *                       'city'	string
     *                       'city[]'	array
     *                       'county'	string
     *                       'county[]'	array
     *                       'address'	string
     *                       'address[]'	array
     *                       'countryIso'	string
     *                       'countryIso[]'	array
     *                       'type'	string
     *                       'type[]'	array
     *                       'postcode'	string
     *                       'postcode[]'	array
     *                       'email'	string
     *                       'email[]'	array
     *                       'status'	string
     *                       'status[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientAddressBookCollection',
        'GET',
        'api/client/client_address_books',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientAddressBook resource.
     *
     * @param Input $Model The new ClientAddressBook resource
     *
     * @return Read
     */
    public function postCollection(Input $Model): Read
    {
        return $this->request(
        'postClientAddressBookCollection',
        'POST',
        'api/client/client_address_books',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientAddressBook resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientAddressBookItem',
        'GET',
        "api/client/client_address_books/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientAddressBook resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated ClientAddressBook resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putClientAddressBookItem',
        'PUT',
        "api/client/client_address_books/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientAddressBook resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientAddressBookItem',
        'DELETE',
        "api/client/client_address_books/$id",
        null,
        [],
        []
        );
    }
}
