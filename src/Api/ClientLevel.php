<?php

namespace WarehouseX\Client\Api;

use WarehouseX\Client\Model\ClientLevel\ClientLevelInput\ClientLevel\Put as Put;
use WarehouseX\Client\Model\ClientLevel\ClientLevelInput\ClientLevel\Write as Write;
use WarehouseX\Client\Model\ClientLevel\ClientLevelOutput\ClientLevel\Read as Read;

class ClientLevel extends AbstractAPI
{
    /**
     * Retrieves the collection of ClientLevel resources.
     *
     * @param array $queries options:
     *                       'page'	integer	The collection page number
     *                       'itemsPerPage'	integer	The number of items per page
     *                       'id'	integer
     *                       'id[]'	array
     *                       'name'	string
     *                       'name[]'	array
     *                       'note'	string
     *                       'status'	string
     *                       'status[]'	array
     *                       'currencyCode'	string
     *                       'currencyCode[]'	array
     *                       'userIdAdmin'	integer
     *                       'userIdAdmin[]'	array
     *                       'createTime[before]'	string
     *                       'createTime[strictly_before]'	string
     *                       'createTime[after]'	string
     *                       'createTime[strictly_after]'	string
     *                       'order[id]'	string
     *                       'order[createTime]'	string
     *                       'amount[between]'	string
     *                       'amount[gt]'	string
     *                       'amount[gte]'	string
     *                       'amount[lt]'	string
     *                       'amount[lte]'	string
     *
     * @return Read[]|null
     */
    public function getCollection(array $queries = []): ?array
    {
        return $this->request(
        'getClientLevelCollection',
        'GET',
        'api/client/client_levels',
        null,
        $queries,
        []
        );
    }

    /**
     * Creates a ClientLevel resource.
     *
     * @param Write $Model The new ClientLevel resource
     *
     * @return Read
     */
    public function postCollection(Write $Model): Read
    {
        return $this->request(
        'postClientLevelCollection',
        'POST',
        'api/client/client_levels',
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Retrieves a ClientLevel resource.
     *
     * @param string $id Resource identifier
     *
     * @return Read|null
     */
    public function getItem(string $id): ?Read
    {
        return $this->request(
        'getClientLevelItem',
        'GET',
        "api/client/client_levels/$id",
        null,
        [],
        []
        );
    }

    /**
     * Replaces the ClientLevel resource.
     *
     * @param string $id    Resource identifier
     * @param Put    $Model The updated ClientLevel resource
     *
     * @return Read
     */
    public function putItem(string $id, Put $Model): Read
    {
        return $this->request(
        'putClientLevelItem',
        'PUT',
        "api/client/client_levels/$id",
        $Model->getArrayCopy(),
        [],
        []
        );
    }

    /**
     * Removes the ClientLevel resource.
     *
     * @param string $id Resource identifier
     *
     * @return mixed
     */
    public function deleteItem(string $id): mixed
    {
        return $this->request(
        'deleteClientLevelItem',
        'DELETE',
        "api/client/client_levels/$id",
        null,
        [],
        []
        );
    }

    /**
     * Retrieves a Client resource.
     *
     * @param string $id Client identifier
     *
     * @return Read|null
     */
    public function api_clients_client_level_get_subresourceClientSubresource(string $id): ?Read
    {
        return $this->request(
        'api_clients_client_level_get_subresourceClientSubresource',
        'GET',
        "api/client/clients/$id/client_level",
        null,
        [],
        []
        );
    }
}
